package com.trevcavill.songselector;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.trevcavill.songselector.R;

public class MainActivity extends AppCompatActivity {

    MediaPlayer player;
    SharedPreferences sharedPrefs;
    SharedPreferences.Editor prefsEditor;
    SeekBar seekBar;
    ImageView playBtn;
    Button chooseBtn;
    Button stopBtn;

    Uri selectedSong;
    String mySong;
    int mProgress;
    Runnable runnable;
    Handler mainHandler;

    public static final String LOG_TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPrefs = getSharedPreferences("PREFS", MODE_PRIVATE);
        mySong = sharedPrefs.getString("song", "");
        if (!mySong.equals("")) {
            selectedSong = Uri.parse(mySong);
            Log.v(LOG_TAG, mySong);
        }

        playBtn = findViewById(R.id.play_button);
        chooseBtn = findViewById(R.id.choose_song_button);
        stopBtn = findViewById(R.id.stop_button);
        seekBar = findViewById(R.id.seek_bar);

        mainHandler = new Handler();

        if (selectedSong != null) {
            player = MediaPlayer.create(MainActivity.this, selectedSong);

            if(player != null){
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                seekBar.setMax(player.getDuration());
                player.seekTo(0);
            }

            else {
                Toast.makeText(MainActivity.this, "Failed to create Media Player.", Toast.LENGTH_SHORT).show();
            }

        }

        else {
            chooseSong();
            if(selectedSong != null) {
                player = MediaPlayer.create(MainActivity.this, selectedSong);
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                seekBar.setMax(player.getDuration());
                player.seekTo(0);

            }
        }

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean input) {
                if (input) {
                    if (player != null) {
                        player.seekTo(progress);
                    }
                    mProgress = progress;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedSong == null){
                    chooseSong();
                }

                else {
                    if(player == null){
                        if(selectedSong != null){
                            player = MediaPlayer.create(MainActivity.this, selectedSong);
                            if(player != null){
                                seekBar.setMax(player.getDuration());
                            }
                            else{
                                chooseSong();
                            }

                        }
                        else{
                            Toast.makeText(MainActivity.this, "Please select a song file to play.", Toast.LENGTH_SHORT).show();
                        }

                    }

                    else{
                        seekBar.refreshDrawableState();

                        if(mProgress > (player.getDuration() - 30)){
                            player.seekTo(0);
                        }

                        if(player.isPlaying()){
                            player.pause();
                            setPlayButtonImage();
                        }

                        else {
                            player.start();
                            playCycle();
                            setPlayButtonImage();
                        }

                        //Restarts song from beginning
                        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                player.seekTo(0);
                                seekBar.setProgress(0);
                                player.start();
                                playCycle();
                            }
                        });
                    }
                }
            }
        });

        chooseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seekBar.setProgress(0);
                chooseSong();
                stopPlaying();

            }
        });

        stopBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                stopPlaying();
                if(selectedSong != null){
                    player = MediaPlayer.create(MainActivity.this, selectedSong);
                    seekBar.setMax(player.getDuration());
                    seekBar.setProgress(0);
                    mProgress = 0;
                    setPlayButtonImage();
                }

            }
        });
    }

    //Changes the play button depending on whether the song is playing or not
    private void setPlayButtonImage(){
        if(player.isPlaying()){
            playBtn.setImageResource(R.drawable.pause_button);
        }

        else {
            playBtn.setImageResource(R.drawable.play_button);
        }
    }

    public void playCycle() {
        seekBar.setProgress(player.getCurrentPosition());

        if(player.isPlaying()){
            runnable = new Runnable() {
                @Override
                public void run() {
                    playCycle();
                }
            };
            mainHandler.postDelayed(runnable, 1000);
        }
    }


    private void stopPlaying(){
        if(player != null){
            player.stop();
            player.reset();
            player.release();
            player = null;
        }
    }

    //Creates dialog box for user to select an audio file from internal storage
    private void chooseSong() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        AlertDialog songDialog = builder.create();
        songDialog.setTitle(R.string.select_song);
        songDialog.setMessage(getString(R.string.choose_song));

        songDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_browse), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent audiofile_chooser_intent = new Intent();
                audiofile_chooser_intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                audiofile_chooser_intent.setType("audio/*");
                audiofile_chooser_intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(audiofile_chooser_intent, 1);
            }
        });

        songDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.button_cancel), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                //Do nothing
            }
        });

        songDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){

        if(requestCode == 1){

            if(resultCode == RESULT_OK){
                selectedSong = data.getData();

                if(selectedSong != null){
                    mySong = selectedSong.toString();
                }

                //Save the Song Uri in string format to Shared Preferences
                prefsEditor = sharedPrefs.edit();
                prefsEditor.putString("song", mySong);
                prefsEditor.apply();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(player != null){
            player.pause();
            setPlayButtonImage();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(player != null){
            player.release();
        }
        mainHandler.removeCallbacks(runnable);
    }

}