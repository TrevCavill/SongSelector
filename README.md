# SongSelector

<img src="app/src/main/res/drawable-v24/ic_songselector.jpg" align="left" width="200" hspace="10" vspace="10">
<br/><br/><br/>SongSelector is an app I am working on to practice different ways of playing audio files from an Android phone's internal storage.<br/>

<br/><br/><br/><br/><br/><br/>

## Screenshots

<div style="display:flex;" >
<img  src="screenshots/SongSelector_MainActivity.jpg" width="45%" >
<img style="margin-left:10px;" src="screenshots/SongSelector_MainActivity2.jpg" width="45%" >
</div>
